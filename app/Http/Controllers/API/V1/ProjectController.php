<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends BaseController
{

    protected $Project = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->middleware('auth:api');
        $this->project = $project;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->project->latest()->paginate(10);

        return $this->sendResponse($projects, 'Project list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Projects\ProjectRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $project = new Project;

        $project->name = $request->input('name');
        $project->description = $request->input('description');
        $project->link = $request->input('link');
        $project->status  = $request->input('status');

        if ($request->hasFile('video')) {
            $pathVideo = $request->file('video')->store('videos', 'public');
            $project->video = $pathVideo;
        }

        if ($request->hasFile('image')) {
            $pathImage = $request->file('image')->store('images', 'public');
            $project->image = $pathImage;
        }

        if ($request->hasFile('document')) {
            $pathDocument = $request->file('document')->store('documents', 'public');
            $project->document = $pathDocument;
        }

        $project->save();


        return $this->sendResponse($project, 'Project Created Successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $Project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Project = $this->project->findOrFail($id);

        $Project->update($request->all());

        return $this->sendResponse($Project, 'Project Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project = $this->project->findOrFail($id);

        $project->delete();

        return $this->sendResponse($project, 'Project has been Deleted');
    }
}
