export default [
    { path: '/profile',   component: require('./components/Profile.vue').default },
    { path: '/analysis',  component: require('./components/Analysis.vue').default },
    { path: '/users',     component: require('./components/Users.vue').default },
    { path: '/projects',  component: require('./components/Projects.vue').default },
];
